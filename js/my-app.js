// Initialize App  
var myApp = new Framework7();
        
// Initialize View          
var mainView = myApp.addView('.view-main',{
	domCache:true
})     
    
        
// Load about page:
mainView.router.load({pageName: 'about'});
       
        
// Go back on main View
mainView.router.back();